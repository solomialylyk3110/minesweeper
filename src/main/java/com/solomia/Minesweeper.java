package com.solomia;

public class Minesweeper {
    public String name="sol";
    private int M = 3;
    final int N = 3;
    final double p = 0.2;
    boolean[][] bombs = new boolean[M+2][N+2];

    public int getM() {
        return M;
    }

    public int getN() {
        return N;
    }

    public Minesweeper() {
        for (int i = 1; i <= M; i++)
            for (int j = 1; j <= N; j++)
                bombs[i][j] = (Math.random() < p);
    }
//    public void print() { // My
//        for (int i = 1; i <= M; i++) {
//            for (int j = 1; j <= N; j++)
//                System.out.print( bombs[i][j]);
//            System.out.println();
//        }
//    }

    public void printGame() {
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++)
                if (bombs[i][j]) System.out.print("* ");
                else             System.out.print(". ");
            System.out.println();
        }
    }

    public void playComputer (){
        int[][] sol = new int[M+2][N+2];
        for (int i = 1; i <= M; i++)
            for (int j = 1; j <= N; j++)
                for (int ii = i - 1; ii <= i + 1; ii++)
                    for (int jj = j - 1; jj <= j + 1; jj++)
                        if (bombs[ii][jj]) sol[i][j]++;

        System.out.println();
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++)
                if (bombs[i][j]) System.out.print("* ");
                else             System.out.print(sol[i][j] + " ");
            System.out.println();
        }

    }
}
