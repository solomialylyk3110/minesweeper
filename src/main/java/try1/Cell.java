package try1;

public class Cell {
    private int x;
    private int y;
    private int value;
    private State state;

    public Cell() {

    }

    public Cell(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public Cell(State state) {
        this.state = state;
        state.setHidden(true);
        state.setMarked(false);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Cell(int x, int y, int value, State state) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.state = state;
        state.setHidden(true);
        state.setMarked(false);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return value + " ";
    }
}
