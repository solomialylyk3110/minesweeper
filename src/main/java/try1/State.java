package try1;

public class State {
    private boolean isMarked;
    private  boolean isHidden;

    public State(boolean isMarked, boolean isHidden) {
        this.isMarked = isMarked;
        this.isHidden = isHidden;
    }

    public boolean isMarked() {
        return isMarked;
    }

    public void setMarked(boolean marked) {
        isMarked = marked;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }
}
