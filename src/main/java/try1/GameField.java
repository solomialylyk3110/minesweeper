package try1;

public class GameField implements Field{
    private final int M = 5;
    private final int N = 5;
    private final double p = 0.3;
    public Cell[][] gameField;
    private boolean[][] bombsField = new boolean[M + 2][N + 2];

    public GameField() {
        gameField = new Cell[M][N];
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                gameField[i][j] = new Cell(i, j, 0);
            }
        }
        setBombs();
        System.out.println("FieldFullFilling()");
        FieldFullFilling();
        System.out.println("showField()");
        showField();
    }



    private void setBombs() {
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++) {
                bombsField[i][j] = (Math.random() < p);
                System.out.print(bombsField[i][j]);
            }
            System.out.println();
        }
    }

    public void FieldFullFilling (){
        int[][] sol = new int[M+2][N+2];
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++) {
                for (int ii = i - 1; ii <= i + 1; ii++) {
                    for (int jj = j - 1; jj <= j + 1; jj++) {
                        if (bombsField[ii][jj]) {
                            sol[i][j]++;
                        }
                    }
                }
            }
        }

        System.out.println("sol");
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++) {
                System.out.print(sol[i][j] + " ");
            }
            System.out.println();
        }

        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++)
                if (bombsField[i][j]) {
                    setCellValue(--i, --j, 9); // change to -1 when everything will be okey
                    i++;
                    j++;
                }
                else {
                    i--;
                    j--;
                    setCellValue(i, j, sol[++i][++j]);
                }
        }
    }

    public Cell getCell(int x, int y) {
        return gameField[x][y];
    }

    public void setCellValue(int x, int y, int value) {
        getCell(x, y).setValue(value);
    }

    public int getCellValue(int x, int y) {
        return getCell(x, y).getValue();
    }

    @Override
    public void showField() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(gameField[i][j]);
            }
            System.out.println();
        }
    }


}
